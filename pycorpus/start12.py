import glob
from nltk.corpus import stopwords

corpus_dir = '/home/jsantillana/university/topicos/basic/2gm*'
corpus_filenames = glob.glob(corpus_dir)
oxford_dict = {}

def oxfordLoad():
    filename = '/home/jsantillana/university/topicos/wordlist.10000'
    with open(filename) as file:
        line = file.readline()
        while line:
            word = line.strip().split()
            line = file.readline()
            oxford_dict[word[0]] = True

print("-----Corriendo-----")
oxfordLoad()
for filename in corpus_filenames:
    t = {}
    print("El archivo es {} -".format(filename))
    out_filename = filename.replace("basic", "vfinal")
    out_file = open(out_filename, "w+")
    with open(filename) as file:
        line = file.readline()
        i = 0
        while line:
            i += 1
            if (i % 100000) == 0:
                print ("Procesado {} lineas.".format(i))
            data = line.strip().split()
            line = file.readline()
            if data[0] in oxford_dict and data[1] in oxford_dict:
                out_file.write("{}\t{}\t{}\n".format(data[0], data[1], data[2]))
        out_file.close()
print("Terminado")
