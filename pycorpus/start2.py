import glob
from nltk.corpus import wordnet as wn
import spacy

corpus_dir = '/home/jsantillana/university/topicos/out/2gm*'
corpus_filenames = glob.glob(corpus_dir)

nlp = spacy.load('en')

print("-----Corriendo-----")

for filename in corpus_filenames:
    t = {}
    print("El archivo es {} -".format(filename))
    with open(filename) as file:
        line = file.readline()
        i = 0
        while line:
            i += 1
            if (i % 100000) == 0:
                print ("Procesado {} lineas.".format(i))
            data = line.strip().split()
            line = file.readline()
            try:
                a = wn.synsets(data[0])
                b = wn.synsets(data[1])
            except IndexError:
                print("Error al verificar si es palabra")
                continue
            c = data[0]
            d = data[1]
            if a or b:
                #print("Al menos una de estas {} {} palabras es ingles".format(c, d))
                if a:
                    if c.isnumeric() and not b:
                        continue
                    c = nlp(c)[0].lemma_.lower()
                if b:
                    if d.isnumeric() and not a:
                        continue
                    d = nlp(d)[0].lemma_.lower()
                index = c + chr(16) + d
                index.encode()
                try:
                    if index in t.keys():
                        t[index] += int(data[2])
                    else:
                        try:
                            t[index] = int(data[2])
                        except IndexError:
                            print("c: {} , d: {}".format(c, d))
                except IndexError:
                    print("problema en if c: {} , d: {}".format(c, d))
    out_filename = filename.replace("out", "bd")
    print("El archivo de salida es {} -".format(out_filename))
    with open(out_filename, "w+") as file:
        i = 0
        for data in t:
            i += 1
            index = data.split(chr(16))
            if (i % 100000) == 0:
                print("Guardado {} lineas.".format(i))
            file.write("{}\t{}\t{}\n".format(index[0], index[1], t[data]))
print("Terminado")
