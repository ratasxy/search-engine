<?php

namespace App\Trie;

use App\Trie\Node;

class Trie2 {
    /** @var Node $head */
    private $head;

    public function __construct()
    {
        $this->head = array(
            "children" => array(),
            "value" => 0
        );
    }

    public function insert(string $index, int $value): bool {
        $current = &$this->head;

        $count = strlen($index);

        for($i=0; $i < $count; $i++) {
            if(!isset($current['children'][$index[$i]])){
                $current['children'][$index[$i]] = array(
                    "children" => array(),
                    "value" => 0
                );
            }

            $current = &$current['children'][$index[$i]];
        }

        $current['value'] += $value;

        return true;
    }

}