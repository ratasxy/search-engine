import plyvel
import json
import zmq

base = '/opt/data'
index = plyvel.DB(base + '/index', create_if_missing=True)
rindex = plyvel.DB(base + '/rindex', create_if_missing=True)
result = plyvel.DB(base + '/result', create_if_missing=True)

li = [13993,
6286,
6157,
596,
6287,
3725,
599,
5791,
2989,
9413,
600,
4495,
10367,
4126,
601,
150,
604,
605,
6294,
2990,
3730,
3731,
606]

for num in li:
    print(rindex.get(str(num).encode()).decode())