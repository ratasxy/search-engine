import glob
from nltk.corpus import stopwords

corpus_dir = '/home/jsantillana/university/topicos/min/2gm*'
corpus_filenames = glob.glob(corpus_dir)

print("-----Corriendo-----")

for filename in corpus_filenames:
    t = {}
    print("El archivo es {} -".format(filename))
    out_filename = filename.replace("min", "basic")
    out_file = open(out_filename, "w+")
    with open(filename) as file:
        line = file.readline()
        i = 0
        while line:
            i += 1
            if (i % 100000) == 0:
                print ("Procesado {} lineas.".format(i))
            data = line.strip().split()
            line = file.readline()
            if data[0].isalpha() and data[1].isalpha():
                if len(data[0]) > 1 and len(data[1]) > 1:
                    out_file.write("{}\t{}\t{}\n".format(data[0], data[1], data[2]))
        out_file.close()
print("Terminado")
