import plyvel
import json
from pprint import pprint
import operator
from scipy import spatial
import math
#import zmq

base = '/home/jsantillana/university/topicosfinal'
index = plyvel.DB(base + '/index', create_if_missing=True)
rindex = plyvel.DB(base + '/rindex', create_if_missing=True)
result = plyvel.DB(base + '/result', create_if_missing=True)
vectorv = plyvel.DB(base + '/vectorv', create_if_missing=True)

mem = {}

def resolve_word_id(word):
    value = index.get(word.encode())
    if value:
        return value
    return False

def translate_id_word(data):
    result = []
    for wid in data:
        value = rindex.get(str(wid[0]).encode())
        result.append([value.decode(), wid[1]])
    return result

def take(dict, n):
    i = 0
    ans = []
    for value in dict:
        if i >= n:
            return ans
        ans.append(value)
        i += 1
    return ans

def intersectionv(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def unionv(lst1, lst2):
    a = set(lst1)
    b = set(lst2)
    c = a.union(b)
    return c

def getDistance(v1, v2):
    intersect = len(intersectionv(v1, v2))
    union = len(unionv(v1, v2))
    return intersect / union

def jacc(a, b):
    inter = intersect(a, b)
    sa = len(a)
    sb = len(b)

    return inter / (sa + sb - inter)

def makeA(d, m):
    return [d.get(str(x), 0) for x in range(m+1)]

def makeSimilar(a, b):
    maxA = int(max(a))
    maxB = int(max(b))
    m = max(maxA, maxB)
    return makeA(a, m)

def intersect(v1, v2):
    intee = 0
    for key, value in v1.items():
        if key in v2:
            intee += 1
    return intee

def cosin(a, b):
    up = 0
    sca = 0
    scb = 0

    for x, y in zip(a, b):
        up += x * y
        sca += pow(x,2)
        scb += pow(y,2)

    return up / math.sqrt(sca * scb)


for key1, value1 in vectorv:
    mem[int(key1.decode())] = json.loads(value1.decode())

print ("Memoria cargada")

while True:
    word = input('Ingrese una palabra: ')
    wid = resolve_word_id(word)
    print ("La palabra tiene el id: {}".format(wid) )
    current = mem[int(wid.decode())]
    jaccards = dict()
    for key, value in mem.items():
        if int(wid.decode()) == key:
            continue
        #ans = getDistance(current, value)
        #ans = cosin(makeSimilar(current, value), makeSimilar(value, current))
        ans = jacc(current, value)
        #ans = 1 - spatial.distance.jaccard(makeSimilar(current, value), makeSimilar(value, current))
        jaccards[key] = ans
    sorted_d = sorted(jaccards.items(), key=operator.itemgetter(1), reverse=True)
    n_items = take(sorted_d, 100)
    ans = translate_id_word(n_items)
    pprint(ans)
