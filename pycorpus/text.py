from pprint import pprint

from itertools import islice

def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))

dict1 = {1:True, 5:True, 7:True, 9:True}
dict2 = {1:True, 3:True, 6:True, 9:True}

