<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Wamania\Snowball\StemmerFactory;
/**
 * Class StemmerController
 * @package App\Controller
 *
 * @Route(path="/")
 */
class StemmerController extends AbstractController{
    /**
     * @Route("/", name="stemmer")
     */
    public function stemmer()
    {
        return $this->render('stemmer/index.html.twig');
    }

    /**
     * @Route("/lexer", name="lexer_response", methods={"POST"})
     */
    public function tlexer(Request $request)
    {
        $content = $request->request->get("content");
        //$content = preg_replace('/[^a-zA-Z\s]/', '', $content);

        $context = new \ZMQContext();

        $requester = new \ZMQSocket($context, \ZMQ::SOCKET_REQ);
        $requester->connect("tcp://pycorpus:5555");

        $requester->send($content);
        $reply = $requester->recv();

        $words = json_decode($reply);

        array_pop($words);

        return $this->render('stemmer/answer.html.twig', array(
                "type" => "Lexer",
                "words" =>$words)
        );
    }
}