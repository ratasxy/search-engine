<?php

namespace App\Trie;

use App\Trie\Node;

class Trie {
    /** @var Node $head */
    private $head;

    public function __construct()
    {
        $this->head = new Node();
    }

    public function insert(string $index, int $value): bool {
        $current = &$this->head;

        $count = strlen($index);

        for($i=0; $i < $count; $i++) {
            if(!isset($current->children[$index[$i]]))
                $current->children[$index[$i]] = new Node();

            $current = &$current->children[$index[$i]];
        }

        $current->value += $value;

        return true;
    }

    public function search(string $index, Node &$node): bool {
        $current = $this->head;

        $count = strlen($index);

        for($i=0; $i < $count; $i++) {
            if(!isset($current->children[$index[$i]]))
                return false;

            $current = $current->children[$index[$i]];
        }

        $node = $current;

        return true;
    }
}