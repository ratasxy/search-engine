<?php

require './vendor/autoload.php';

use App\Trie\Trie;
use App\Trie\Node;
use App\Trie\Trie2;

echo "Procesador de Google Corpus\n";

$corpusDir = '/opt/data/';
$corpusFilenames = scandir($corpusDir);

$starttime = microtime(true);

echo "Memoria inicial: " . round(memory_get_usage()/1048576,2) . " MB\n";

$trie = new Trie2();
$db = array();

foreach ($corpusFilenames as $filename) {
    if (!preg_match('/2gm/', $filename))
        continue;

    echo "Procesando archivo $filename...\n";

    $fn = fopen($corpusDir . $filename,"r");

    $i = 0;

    $atime = microtime(true);
    while(! feof($fn))  {
        $line = fgets($fn);
        $contentLine = array();
        $contentLine = preg_split('/(\s|\t)/', $line);

        //echo "Palabra 1: $contentLine[0], Palabra 2: $contentLine[1], Ocurrencias: $contentLine[2]\n";
        $index = $contentLine[0] . chop(16) . $contentLine[1];
        //echo "Procesando $index \n";
        $trie->insert($index, (int) $contentLine[2]);
        //$db[$index] =  (int) $contentLine[2];

        $i++;
        if($i == 1000000){
            $i = 0;

            $endtime = microtime(true);
            $timediff = $endtime - $atime;
            $atime = microtime(true);
            echo "Finalizado en:$timediff Segundos.\n";
            echo "Memoria actual: " . round(memory_get_usage()/1048576,2) . " MB\n";

        }
    }

    fclose($fn);
}

$endtime = microtime(true);
$timediff = $endtime - $starttime;
echo "Finalizado en:$timediff Segundos.\n";
echo "Memoria final: " . round(memory_get_usage()/1048576,2) . " MB\n";