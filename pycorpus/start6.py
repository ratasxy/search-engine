import plyvel
import operator
from pprint import pprint
import itertools

index = plyvel.DB('/home/jsantillana/university/topicos/rindex', create_if_missing=True)
bigram = plyvel.DB('/home/jsantillana/university/topicos/bigram', create_if_missing=True)
result = plyvel.DB('/home/jsantillana/university/topicos/result', create_if_missing=True)

def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(itertools.islice(iterable.iteritems(), n))

def intersectionv(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def unionv(lst1, lst2):
    a = set(lst1)
    b = set(lst2)
    c = a.union(b)
    return c

def getDict(word):
    bword = (str(word.decode()) + chr(30)).encode()
    lst = []
    for key, value in bigram.iterator(prefix=bword):
        tkey = key.decode()
        tkey = int(tkey.replace(bword.decode(), ''))
        lst.append(tkey)
    return lst

def getDistance(v1, v2):
    intersect = len(intersectionv(v1, v2))
    union = len(unionv(v1, v2))
    return intersect / union


for key1, value1 in index:
    k1 = int(key1.decode())
    print("Procesando: {}".format(k1))
    v1 = getDict(key1)
    jaccards = dict()
    for key2, value2 in index.iterator(start=key1, include_start=False):
        k2 = int(key2.decode())
        v2 = getDict(key2)
        ans = getDistance(v1, v2)
        jaccards[k2] = ans
    print("Ordenando")
    sorted_d = sorted(jaccards.items(), key=operator.itemgetter(1), reverse=True)
    n_items = take(10, sorted_d)
    pprint(n_items)
    exit()