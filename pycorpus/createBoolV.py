import plyvel
import json
import operator
from pprint import pprint
import itertools
base = '/home/jsantillana/university/topicosfinal'
index = plyvel.DB(base + '/rindex', create_if_missing=True)
bigram = plyvel.DB(base + '/bigram', create_if_missing=True)
result = plyvel.DB(base + '/result', create_if_missing=True)
vectorv = plyvel.DB(base + '/vectorv', create_if_missing=True)

def getDict(word):
    bword = (str(word.decode()) + chr(30)).encode()
    lst = {}
    for key, value in bigram.iterator(prefix=bword):
        tkey = key.decode()
        tkey = int(tkey.replace(bword.decode(), ''))
        lst[tkey] = int(value.decode())
    return lst

for key1, value1 in index:
    k1 = int(key1.decode())
    print("Procesando: {}".format(k1))
    v1 = getDict(key1)
    txt = json.dumps(v1)
    vectorv.put(key1, txt.encode())