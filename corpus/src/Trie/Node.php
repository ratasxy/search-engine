<?php

namespace App\Trie;

class Node {
    public $children;
    public $value;

    public function __construct()
    {
        $this->children = array();
        $this->value = 0;
    }
}