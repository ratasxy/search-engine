import plyvel
import operator
import json
from pprint import pprint
import itertools

index = plyvel.DB('/home/jsantillana/university/topicos/index', create_if_missing=True)
bigram = plyvel.DB('/home/jsantillana/university/topicos/bigram', create_if_missing=True)
result = plyvel.DB('/home/jsantillana/university/topicos/result', create_if_missing=True)
oxford_dict = []

def take(dict, n):
    i = 0
    ans = []
    for value in dict:
        if i >= n:
            return ans
        ans.append(value)
        i += 1
    return ans

def intersectionv(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def unionv(lst1, lst2):
    a = set(lst1)
    b = set(lst2)
    c = a.union(b)
    return c

def getDict(word):
    bword = (str(word.decode()) + chr(30)).encode()
    lst = []
    for key, value in bigram.iterator(prefix=bword):
        tkey = key.decode()
        tkey = int(tkey.replace(bword.decode(), ''))
        lst.append(tkey)
    return lst

def getDistance(v1, v2):
    intersect = len(intersectionv(v1, v2))
    union = len(unionv(v1, v2))
    return intersect / union

def oxfordLoad():
    filename = '/home/jsantillana/university/topicos/oxford.txt'
    with open(filename) as file:
        line = file.readline()
        while line:
            word = line.strip().split()
            value = index.get(word[0].encode())
            line = file.readline()
            if value:
                if value in oxford_dict:
                    continue
                oxford_dict.append(value)

oxfordLoad()
print(oxford_dict)
for id1 in oxford_dict:
    print("Procesando: {}".format(id1))
    v1 = getDict(id1)
    jaccards = dict()
    t = 0
    for id2 in oxford_dict:
        t += 1
        if id1 == id2:
            continue
        v2 = getDict(id2)
        jaccards[int(id2.decode())] = getDistance(v1, v2)
    sorted_d = sorted(jaccards.items(), key=operator.itemgetter(1), reverse=True)
    n_items = take(sorted_d, 100)
    txt = json.dumps(n_items)
    result.put(id1, txt.encode())