import plyvel
import json
import zmq

base = '/opt/data'
index = plyvel.DB(base + '/index', create_if_missing=True)
rindex = plyvel.DB(base + '/rindex', create_if_missing=True)
result = plyvel.DB(base + '/result', create_if_missing=True)

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

def resolve_word_id(word):
    value = index.get(word.encode())
    if value:
        return value
    return False

def translate_id_word(data):
    result = []
    for wid in data:
        print(wid[0])
        value = rindex.get(str(wid[0]).encode())
        result.append([value.decode(), wid[1]])
    return result

print ("Iniciado Python:")
while True:
    word = socket.recv_string()
    print("Received request: %s" % word)

    wid = resolve_word_id(word)
    ans = []
    if wid:
        wordd = result.get(wid)
        if result:
            data = json.loads(wordd.decode())
            ans = translate_id_word(data)
            print(ans)
        else:
            print ("Aun no calculado")
    else:
        print("palabra no indexada")
    socket.send_string(json.dumps(ans))


