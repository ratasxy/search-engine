import glob
from nltk.corpus import wordnet as wn

corpus_dir = '/home/jsantillana/university/topicos/bd/2gm*'
corpus_filenames = glob.glob(corpus_dir)

print("-----Corriendo-----")

for filename in corpus_filenames:
    t = {}
    print("El archivo es {} -".format(filename))
    out_filename = filename.replace("bd", "final")
    out_file = open(out_file, "w+")
    with open(filename) as file:
        line = file.readline()
        i = 0
        while line:
            i += 1
            if (i % 100000) == 0:
                print ("Procesado {} lineas.".format(i))
            data = line.strip().split()
            line = file.readline()
            try:
                a = wn.synsets(data[0])
                b = wn.synsets(data[1])
            except IndexError:
                print("Error al verificar si es palabra")
                continue
            if a and b:
                c = data[0]
                d = data[1]
                out_file.write("{}\t{}\t{}\n".format(data[0], data[1], data[2]))
    print("Guardado {} lineas.".format(i))
    out_file.close()
print("Terminado")
