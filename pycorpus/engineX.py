import plyvel
import json
from pprint import pprint
import operator
#import zmq

base = '/home/jsantillana/university/topicosmin'
index = plyvel.DB(base + '/index', create_if_missing=True)
rindex = plyvel.DB(base + '/rindex', create_if_missing=True)
result = plyvel.DB(base + '/result', create_if_missing=True)
vector = plyvel.DB(base + '/vector', create_if_missing=True)

mem = {}

def resolve_word_id(word):
    value = index.get(word.encode())
    if value:
        return value
    return False

def translate_id_word(data):
    result = []
    for wid in data:
        print(wid[0])
        value = rindex.get(str(wid[0]).encode())
        result.append([value.decode(), wid[1]])
    return result

def take(dict, n):
    i = 0
    ans = []
    for value in dict:
        if i >= n:
            return ans
        ans.append(value)
        i += 1
    return ans

def intersectionv(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def unionv(lst1, lst2):
    a = set(lst1)
    b = set(lst2)
    c = a.union(b)
    return c

def getDistance(v1, v2):
    intersect = len(intersectionv(v1, v2))
    union = len(unionv(v1, v2))
    return intersect / union


for key1, value1 in vector:
    mem[int(key1.decode())] = json.loads(value1.decode())

print ("Memoria cargada")

while True:
    word = input('Ingrese una palabra: ')
    wid = resolve_word_id(word)
    print ("La palabra tiene el id: {}".format(wid) )
    current = mem[int(wid.decode())]
    jaccards = dict()
    i = 0
    for key, value in mem.items():
        i = i + 1
        if i % 100 == 0:
            print("Procesado {}".format(i))
        if wid == key:
            continue
        ans = getDistance(current, value)
        jaccards[key] = ans
    sorted_d = sorted(jaccards.items(), key=operator.itemgetter(1), reverse=True)
    n_items = take(sorted_d, 100)
    ans = translate_id_word(n_items)
    pprint(ans)
