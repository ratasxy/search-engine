import plyvel
import json
import operator
from pprint import pprint
import itertools

index = plyvel.DB('/home/jsantillana/university/topicosmin/rindex', create_if_missing=True)
bigram = plyvel.DB('/home/jsantillana/university/topicosmin/bigram', create_if_missing=True)
result = plyvel.DB('/home/jsantillana/university/topicosmin/result', create_if_missing=True)
vector = plyvel.DB('/home/jsantillana/university/topicosmin/vector', create_if_missing=True)

def getDict(word):
    bword = (str(word.decode()) + chr(30)).encode()
    lst = []
    for key, value in bigram.iterator(prefix=bword):
        tkey = key.decode()
        tkey = int(tkey.replace(bword.decode(), ''))
        lst.append(tkey)
    return lst


for key1, value1 in index:
    k1 = int(key1.decode())
    print("Procesando: {}".format(k1))
    v1 = getDict(key1)
    txt = json.dumps(v1)
    vector.put(key1, txt.encode())