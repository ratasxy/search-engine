import glob
import plyvel

corpus_dir = '/home/jsantillana/university/topicos/min/2gm*'
corpus_filenames = glob.glob(corpus_dir)

index = plyvel.DB('/home/jsantillana/university/topicos/index', create_if_missing=True)
rindex = plyvel.DB('/home/jsantillana/university/topicos/rindex', create_if_missing=True)
bigram = plyvel.DB('/home/jsantillana/university/topicos/bigram', create_if_missing=True)
nindex = 1
tindex = open('/home/jsantillana/university/topicos/tindex.txt', "w+")

def get_index(word):
    global nindex
    iword = word.encode()
    value = index.get(iword)
    if value:
        nvalue = int(value.decode())
        return nvalue

    value = nindex
    nindex = nindex + 1
    ivalue = str(value).encode()
    index.put(iword, ivalue)
    rindex.put(ivalue, iword)
    tindex.write("{}\t{}\n".format(word, value))
    return value



print("-----Corriendo-----")

for filename in corpus_filenames:
    t = {}
    print("El archivo es {} -".format(filename))
    out_filename = filename.replace("min", "txtgram")
    out_file = open(out_filename, "w+")
    with open(filename) as file:
        line = file.readline()
        i = 0
        while line:
            i += 1
            if (i % 100000) == 0:
                print ("Procesado {} lineas.".format(i))
            data = line.strip().split()
            line = file.readline()
            aindex = get_index(data[0])
            bindex = get_index(data[1])
            codeindex = str(aindex) + chr(30) + str(bindex)
            bigram.put(codeindex.encode(), str(data[2]).encode())
            out_file.write("{}\t{}\t{}\n".format(aindex, bindex, data[2]))
        out_file.close()
tindex.close()
print("Terminado")
